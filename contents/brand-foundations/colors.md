---
name: Colors
---

While the brand is ever-evolving, the GitLab brand currently consists of six primary colors that are used in a wide array of marketing materials.

![GitLab brand colors](/img/brand/gitlab-hex-rgb-colors.png)
